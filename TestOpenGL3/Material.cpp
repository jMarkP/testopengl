//
//  Material.cpp
//  TestOpenGL3
//
//  Created by Mark Pim on 26/01/2020.
//  Copyright © 2020 Chillios. All rights reserved.
//

#include "Material.h"

Material::Material() {
    specularIntensity = 0.0f;
    shininess = 0.0f;
}

Material::Material(GLfloat sIntensity, GLfloat shine) {
    specularIntensity = sIntensity;
    shininess = shine;
}

void Material::UseMaterial(GLuint specularIntensityLocation, GLuint shininessLocation) {
    glUniform1f(specularIntensityLocation, specularIntensity);
    glUniform1f(shininessLocation, shininess);
}

Material::~Material() {
    
}
