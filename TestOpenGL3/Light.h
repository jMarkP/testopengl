//
//  Light.hpp
//  TestOpenGL3
//
//  Created by Mark Pim on 25/01/2020.
//  Copyright © 2020 Chillios. All rights reserved.
//

#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>

class Light
{
public:
    Light();
    Light(GLfloat red, GLfloat green, GLfloat blue, GLfloat aIntensity,
          GLfloat x, GLfloat y, GLfloat z, GLfloat dIntensity);
    
    void UseLight(GLuint ambientIntensityLocation, GLuint ambientColourLocation,
                  GLuint diffuseIntensityLocation, GLuint directionLocation);
    
    ~Light();
    
private:
    glm::vec3 colour;
    GLfloat ambientIntensity;
    
    glm::vec3 direction;
    GLfloat diffuseIntensity;
};
