//
//  Light.cpp
//  TestOpenGL3
//
//  Created by Mark Pim on 25/01/2020.
//  Copyright © 2020 Chillios. All rights reserved.
//

#include "Light.h"

Light::Light() {
    colour = glm::vec3(1.0f, 1.0f, 1.0f);
    direction = glm::vec3(0.0f, -1.0f, 0.0f);
    ambientIntensity = 1.0f;
    diffuseIntensity = 0.0f;
}

Light::Light(GLfloat red, GLfloat green, GLfloat blue, GLfloat aIntensity,
             GLfloat x, GLfloat y, GLfloat z, GLfloat dIntensity) {
    colour = glm::vec3(red, green, blue);
    direction = glm::vec3(x, y, z);
    ambientIntensity = aIntensity;
    diffuseIntensity = dIntensity;
}

void Light::UseLight(GLuint ambientIntensityLocation, GLuint ambientColourLocation,
                     GLuint diffuseIntensityLocation, GLuint directionLocation) {
    glUniform3f(ambientColourLocation, colour.x, colour.y, colour.z);
    glUniform3f(directionLocation, direction.x, direction.y, direction.z);
    glUniform1f(ambientIntensityLocation, ambientIntensity);
    glUniform1f(diffuseIntensityLocation, diffuseIntensity);
}

Light::~Light() {
    
}
